// dependencies
const AWS = require('aws-sdk');

// get reference to S3 client
const s3 = new AWS.S3();

exports.handler = async (event, context, callback) => {
    const params = {
        Bucket: process.env.BUCKET,
        Key: process.env.KEY
    };
    
    if (!event.requestContext.authorizer) {
      errorResponse('Authorization not configured', context.awsRequestId, callback);
      return;
    }

    // Because we're using a Cognito User Pools authorizer, all of the claims
    // included in the authentication token are provided in the request context.
    // This includes the username as well as other attributes.
    const username = event.requestContext.authorizer.claims['cognito:username'];
    await s3.getObject(params).promise().then((data) => {
        const chain = JSON.parse(data.Body.toString('utf-8')).chain;
        let balance = 0;
        chain.forEach(entry => {
            entry.transactions.forEach(t => {
                if (t.username == username && t.amount) {
                    balance += t.amount;
                }
            });
        });
        
        callback(null, {
            statusCode: 200,
            body: JSON.stringify({username, balance}),
            headers: {
                'Access-Control-Allow-Origin': '*',

            },
        });
    }).catch((err) => {
        console.error(err);
    });
};

function errorResponse(errorMessage, awsRequestId, callback) {
  callback(null, {
    statusCode: 500,
    body: JSON.stringify({
      Error: errorMessage,
      Reference: awsRequestId,
    }),
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  });
}
