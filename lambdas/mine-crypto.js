// dependencies
const AWS = require('aws-sdk');

// get reference to S3 client
const s3 = new AWS.S3();
const s3_params = {
    Bucket: process.env.BUCKET,
    Key: process.env.KEY
};

const sqs = new AWS.SQS();
const QUEUE_URL = process.env.SQS_QUEUE_URL;

const sqs_params = {
 AttributeNames: [
    "SentTimestamp"
 ],
 MaxNumberOfMessages: 10,
 MessageAttributeNames: [
    "All"
 ],
 QueueUrl: QUEUE_URL,
 VisibilityTimeout: 20,
 WaitTimeSeconds: 0
};

exports.handler = async (event, context, callback) => {
    await s3.getObject(s3_params).promise().then(async(data) => {
        
        const chain = JSON.parse(data.Body.toString('utf-8')).chain;
        const transactions = [];
        const sqsReceivePromise = sqs.receiveMessage(sqs_params).promise();
        let received = await sqsReceivePromise;
        
        let hash;
        
        if (!received.Messages) {
          await new Promise(r => setTimeout(r, 1000));
          received = await sqsReceivePromise;
        }

        if (received.Messages) {
          received.Messages.forEach((message) => {
            transactions.push(JSON.parse(message.Body));
          });
  
          hash = mine(transactions, chain);
          deleteMessages(received);
          await updateChain(chain);
        }

        
        callback(null, {
            statusCode: 200,
            body: { hash },
            headers: {
                'Access-Control-Allow-Origin': '*',
                
            },
        });
    }).catch((err) => {
        console.error(err);
    });
};

function deleteMessages(data) {
  const deleteParams = {
    QueueUrl: QUEUE_URL,
    ReceiptHandle: data.Messages[0].ReceiptHandle
  };
        
  sqs.deleteMessage(deleteParams, function(err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("Success", data.MessageId);
    }
  });
}

function updateChain(chain) {
  const params = Object.assign({
    Body: JSON.stringify({
      chain
    }),
    ContentType: "application/json"
  }, s3_params);
  return s3.putObject(params).promise();
}

const crypto = require('crypto');

function mine(transactions, chain) {
  if (!transactions) {
    return false;
  } else {
    const lastBlock = chain[chain.length - 1];
    const newBlock = {
      index: lastBlock.index + 1,
      transactions,
      timestamp: Date.now(),
      previous_hash: lastBlock.hash
    }
    
    const proof = proofOfWork(newBlock);
    newBlock.hash = proof;
    chain.push(newBlock);
    return proof;
  }
}

function blockComputeHash(block) {
  let code = JSON.stringify(block);
  const hash = crypto.createHash('sha256');
  code = hash.update(code);
  return hash.digest('hex');
}

function proofOfWork(block) {
  block.nonce = 0;
  let computedHash = blockComputeHash(block);
  while (!computedHash.startsWith('000')) {
    block.nonce++;
    computedHash = blockComputeHash(block);
  }
  return computedHash;
}